﻿namespace SamanageTesting.Model
{
    class SamanageCentres
    {
        public class BusinessRecord
        {
            public int id { get; set; }
            public string name { get; set; }
            public string description { get; set; }
        }

        public class Site
        {
            public int id { get; set; }
            public string name { get; set; }
            public string location { get; set; }
            public string description { get; set; }
            public object time_zone { get; set; }
            public object language { get; set; }
            public BusinessRecord business_record { get; set; }
        }
    }
}
