﻿using RestSharp;
using SamanageTesting.Model;
using System;
using System.Collections.Generic;

namespace SamanageTesting
{
    /// <summary>
    /// </summary>
    internal class Program
    {
        private static void Main(string[] args)
        {
            var siteList = new List<SamanageCentres.Site>();
            var client = new RestClient("https://api.samanage.com");

            var request = new RestRequest("sites.json", Method.GET);
            request.AddParameter("page", "1");


            for (int i = 0; i < 39; i++)
            {
                request.AddParameter("page", i);

                request.AddHeader("Authorization", "Basic U2FtYW5hZ2UuQVBJQHJlZ3VzLmNvbTpRazZYa0pMa0FRU2V3dWFj");

                var response = client.Execute<List<SamanageCentres.Site>>(request);
                siteList.InsertRange(siteList.Count, response.Data);
                request.Parameters.Clear();

            }

            System.IO.StreamWriter file =
             new System.IO.StreamWriter("..\\SamanageSites.txt", true);

            Console.Write(siteList.Count + " Sites Found.  ");
            Console.WriteLine("Press any key to write Details to ..\\SamanageSites.txt file");
            Console.ReadLine();
            foreach (var site in siteList)
            {
                file.WriteLine("ID:" + site.id + " name: " + site.name);
            }

            file.Close();

        }
    }
}
